import java.util.Arrays;

/**
 * 
 */

/**
 * @author EALARCON
 *
 */
public class Array2ETestApp 
{

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		/*
		//Create a 3 * 2 Array of ints
		int[] [] numbersTable = new int[3] [2]; //3 rows, 2 col
		numbersTable[0] [0] = 1;
		numbersTable[0] [1] = 2;
		numbersTable[1] [0] = 3;
		numbersTable[1] [1] = 4;
		numbersTable[2] [0] = 5;
		numbersTable[0] [1] = 6;
		*/
		
		int[] [] numbersTable = { { 1, 2 }, { 3, 4 }, { 5, 6 } };
		
		//Create a "jagged" Array
		int[] [] jaggedNumbersTable = { { 1, 2 }, { 3 }, {4, 5, 6 } };
		
		//Create a string of Multi-Dimensional Array
		System.out.println( Arrays.deepToString( numbersTable ) );
		System.out.println( Arrays.deepToString( jaggedNumbersTable ) );
		
		System.out.println( Arrays.toString( numbersTable[0] ) );
		System.out.println( Arrays.toString( numbersTable[1] ) );
		System.out.println( Arrays.toString( numbersTable[2] ) );
		
		for( int rowIndex = 0; rowIndex < numbersTable.length; rowIndex++ ) //arrayName.length is equal # of rows
		{
			//For rectangular arrays, arrayName[0.length = # of columns]
			for( int colIndex = 0; colIndex < numbersTable[0].length; colIndex++ )
			{
				//For looping through column elements OF THE CURRENT ROW
				System.out.print( numbersTable[rowIndex][colIndex] + "\t" );				
				
			} //end nested for loop
			//Next Line Output
			System.out.println();
			
		} //end out for loop
		
		//Use enhanced for loop to do the same thing
		for( int[] row : numbersTable )
		{
			for( int columnValue : row )
			{
				System.out.print( columnValue + "\t" );
			} //end nested for loop
			System.out.println();
		} //end outer for loop
			
	} //end method main

} //end class Array2ETestApp
